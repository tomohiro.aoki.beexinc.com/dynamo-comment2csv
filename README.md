# dynamo-comment2csv

[概要]

```
・DynamoDB対象テーブルからCSV形式でデータを取り出す。
```

[DynamoDB対象テーブル]

```
・ise-km-backend-comments
・ise-km-backend-workspace-card-comments
```

[出力CSV]

```
・article_comments
・article_comment_reactions
・article_highlights
・workspace_card_comments
・workspace_card_comment_reactions
```


【実行方法】

```
[事前インポート]
>pip install pandas boto3

[本番実行]
>python comment2csv.py prod
>python workspace_card_comment2csv.py prod
[STG実行]
>python comment2csv.py stg
>python workspace_card_comment2csv.py stg
[LOCAL実行]※ローカル動作確認のみ利用(ローカルDynamoDB要)
>python comment2csv.py local
>python workspace_card_comment2csv.py local
```
