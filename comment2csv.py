# -*- coding: utf-8 -*-
from logging import getLogger
import sys
import function as fn
from botocore.exceptions import ClientError

logger = getLogger(__name__)
table = fn.get_dynamodb_table(sys.argv, 'ise-km-backend-comments')


def main():
    logger.debug("start")

    try:
        response = table.scan()
        fn.edit_header('article_comments', 'article_comment_reactions', 'article_highlights',
                       'article_meta', 'article_comment_meta')
        for item in response['Items']:
            edit_item(item)

        while 'LastEvaluatedKey' in response:
            response = table.scan(
                 ExclusiveStartKey=response['LastEvaluatedKey']
            )

            for item in response['Items']:
                edit_item(item)

    except ClientError as err:
        logger.exception(err)
        raise err
    except Exception as err:
        logger.exception(err)
        raise err

    return "success!"


def edit_item(item):

    article_meta_list = list()
    c_cnt = len(item['comments'].items()) if 'comments' in item else 0
    h_cnt = len(item['highlight_comments'].items()) if 'highlight_comments' in item else 0
    last_c_id = ''
    last_h_id = ''
    max_created_at_timestamp = 0

    # comment
    if 'comments' in item:
        comment_list = list()
        comment_reaction_list = list()
        for ck, cv in item['comments'].items():
            article_comment_meta_list = list()
            err = fn.edit_user(cv)
            if err:
                print('comment: not found user_serial_id and user_serial_id:{}'.format(ck))
                continue
            fn.add_comment(cv, item['article_id'], item['contract_code'], comment_list)

            # last_comment_id
            now_created_at_timestamp = int(cv['created_at']['Seconds']) * fn.BILLION + int(cv['created_at']['Nanos'])
            if max_created_at_timestamp < now_created_at_timestamp:
                last_c_id = cv['comment_id']
                max_created_at_timestamp = now_created_at_timestamp

            # comment reaction
            if 'reactions' in cv:
                for rk, rv in cv['reactions'].items():
                    err = fn.edit_user(rv)
                    if err:
                        print('comment reaction: not found user_serial_id and user_serial_id:{}'.format(rk))
                        continue
                    fn.add_reaction(cv, rv, item['article_id'], item['contract_code'], comment_reaction_list)

            # thread count
            t_cnt = len(cv['threads'].items()) if 'threads' in cv else 0
            fn.add_comment_meta(cv['comment_id'], t_cnt, article_comment_meta_list)

            # thread
            if 'threads' in cv:
                for tk, tv in cv['threads'].items():
                    err = fn.edit_user(tv)
                    if err:
                        print('thread: not found user_serial_id and user_serial_id:{}'.format(tk))
                        continue
                    fn.add_thread(cv, tv, item['article_id'], item['contract_code'], comment_list)

                    # thread reaction
                    if 'reactions' in tv:
                        for trk, trv in tv['reactions'].items():
                            err = fn.edit_user(trv)
                            if err:
                                print('comment reaction: not found user_serial_id and user_serial_id:{}'.format(trk))
                                continue
                            fn.add_thread_reaction(tv, trv, item['article_id'], item['contract_code'],
                                                   comment_reaction_list)

            fn.save_csv("article_comment_meta.csv", article_comment_meta_list)

        fn.save_csv("article_comments.csv", comment_list)
        fn.save_csv("article_comment_reactions.csv", comment_reaction_list)

    # highlight comment
    max_created_at_timestamp = 0
    if 'highlight_comments' in item:
        highlight_list = list()
        for hk, hv in item['highlight_comments'].items():
            err = fn.edit_user(hv)
            if err:
                print('comment: not found user_serial_id and user_serial_id:{}'.format(hk))
                continue
            fn.add_highlight_comment(hv, item['article_id'], item['contract_code'], highlight_list)

            # last_highlight_id
            now_created_at_timestamp = int(hv['created_at']['Seconds']) * fn.BILLION + int(hv['created_at']['Nanos'])
            if max_created_at_timestamp < now_created_at_timestamp:
                last_h_id = hv['comment_id']
                max_created_at_timestamp = now_created_at_timestamp

        fn.save_csv("article_highlights.csv", highlight_list)

    # comment_count, highlight_count, last_comment_id, last_highlight_id
    fn.add_meta(item['article_id'], item['contract_code'], c_cnt, h_cnt, last_c_id, last_h_id, article_meta_list)
    fn.save_csv("article_meta.csv", article_meta_list)


if __name__ == "__main__":
    main()